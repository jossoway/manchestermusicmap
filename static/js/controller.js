define([
  'marionette',
  'mediator'
], function(Marionette, Mediator) {

  return {
    index: function() {
      Mediator.request('app:panel:intro');
    },

    video: function(slug) {
      // we need to look up the Event based on the slug
      var events = Mediator.request('search-index:find:slug', slug);
      if(events.length === 0) {
        Mediator.request('app:panel:intro');
      }
      else {
        // get first event - should only be one, and extract first artist name as the query
        var event = events.first();
        var artist = event.get('artists')[0];
        Mediator.request('app:panel:events', artist.name, events);
      }
    },

    search: function(query) {
      var results = Mediator.request('search-index:search', query);
      // update event listing results and searchbox
      Mediator.request('app:panel:events', query, results);
      Mediator.request('searchbox:setQuery', query);
    },

    about: function() {
      Mediator.request('app:panel:about');
    },

    contact: function() {
      Mediator.request('app:panel:contact');
    },

    suggest: function() {
      Mediator.request('app:panel:suggest');
    },

    venues: function() {
      Mediator.request('app:panel:venues');
    },

    artists: function() {
      Mediator.request('app:panel:artists');
    }
  };
});
