define([
  'backbone', 'csrf', 'backbone_validation'
], function(Backbone) {

  return Backbone.Model.extend({
    urlRoot: '/api/suggestions/',

    validation: {
      artists: {
        required: true,
        msg: 'Please provide at least one artist name'
      },
      venue: {
        required: true,
        msg: 'Please provide a venue'
      },
      video: [{
        required: true,
        msg: 'Please provide a video link'
      },
      {
        pattern: 'url',
        msg: 'Please enter a valid URL'
      }],
      submitter_name: {
        required: true,
        msg: 'Please provide your name'
      },
      submitter_email: [{
        required: true,
        msg: 'Please provide your email address'
      }, {
        pattern: 'email',
        msg: 'Please enter a valid email'
      }]
    }
  });
});
