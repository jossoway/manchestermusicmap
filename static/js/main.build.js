({
    baseUrl: ".",
    name: 'main',
    mainConfigFile: 'main.js',
    out: 'main.built.js',

    inlineText: true,

    pragmasOnSave: {
        //removes Handlebars.Parser code (used to compile template strings) set
        //it to `false` if you need to parse template strings even after build
        excludeHbsParser : true,
        // kills the entire plugin set once it's built.
        excludeHbs: true,
        // removes i18n precompiler, handlebars and json2
        excludeAfterBuild: true
    },

    paths: {
      requireLib: 'lib/almond',
      "hbs": "lib/hbs",
      "Handlebars" : "lib/Handlebars",
      "hbs/underscore" : "lib/hbs/underscore",
      "hbs/i18nprecompile" : "lib/hbs/i18nprecompile",
      "hbs/json2" : "lib/hbs/json2"
      // if your project is already using underscore.js and you want to keep
      // the hbs plugin even after build (excludeHbs:false) you should set the
      // "hbs/underscore" path to point to the shared location like
      // "hbs/underscore" : "lib/underscore" to avoid loading it twice
    },

    include: ["requireLib"]
})
