define([
  'jquery',
  'underscore',
  'marionette',
  'mediator',
  'constants',
  'search-index',
  'collections/events',
  'hbs!templates/header',
  'hbs!templates/search-results',
  'typeahead',
  'bootstrap_dropdown'
], function($, _, Marionette, Mediator, Constants, SearchIndex, Events, tpl, resultsTpl) {

  return Backbone.Marionette.ItemView.extend({
    // template: Backbone.Marionette.TemplateCache.get('header'),
    // resultsTemplate: Backbone.Marionette.TemplateCache.get('search-results'),
    template: tpl,
    resultsTemplate: resultsTpl,

    searchTerms: [], // list for auto completion

    navPanel: $('.nav'),

    events: {
      "click #menu-button":   "togglePanel",
      "click .brand":         "goHome",
      "focus #search":        "clearSearch",
      "click #nav-contact":   "displayContact",
      "click #nav-about":   "displayAbout",
      "click #nav-suggest":   "displaySuggest",
      "click #nav-all-artists":   "displayAllArtists",
      "click #nav-all-venues":   "displayAllVenues",
      "click #all-artists-link":   "displayAllArtists",
      "click #all-venues-link":   "displayAllVenues",
    },

    onRender: function() {
      var self = this;

      this.searchbox = this.$el.find('#search');

      Mediator.setHandler("searchbox:addSearchTerm", function(doc) {
        self.addSearchTerm(doc);
      });

      Mediator.setHandler('searchbox:addDataset', function() {
        self.addDataset();
      });

      Mediator.setHandler('searchbox:setQuery', function(query) {
        self.setQuery(query);
      });
    },

    addSearchTerm: function(doc) {
        // avoid dupes
        if (_.findWhere(this.searchTerms, {'name': doc.name})) {
            // do nothing
        }
        else {
            this.searchTerms.push(doc);
        }
    },

    addDataset: function() {
      var self = this;
      var typeahead = this.searchbox.typeahead({
          name: 'results',
          local: this.searchTerms,
          template: this.resultsTemplate
      });

      typeahead.bind('typeahead:selected', function(dataset, datum) {
          // Get value for selected result and use this to perform search
          // of Events
          var query = datum.value;

          var results = Mediator.request('search-index:search', query);

          // update event listing results
          Mediator.request('app:panel:events', query, results);

          // blur search box
          self.searchbox.blur();
      });
    },

    setQuery: function(query) {
      this.searchbox.typeahead('setQuery', query);
    },

    togglePanel: function(e) {
      e.preventDefault();
      Mediator.request('app:togglePanel');
    },

    goHome: function(e) {
      e.preventDefault();
      Mediator.request('app:panel:intro');
    },

    clearSearch: function() {
      Mediator.request('searchbox:setQuery', '');
    },

    displayContact: function(e) {
      e.preventDefault();
      Mediator.request('app:panel:contact');
    },

    displayAbout: function(e) {
      e.preventDefault();
      Mediator.request('app:panel:about');
    },

    displaySuggest: function(e) {
      e.preventDefault();
      Mediator.request('app:panel:suggest');
    },

    displayAllArtists: function(e) {
      e.preventDefault();
      Mediator.request('app:panel:artists');
    },

    displayAllVenues: function(e) {
      e.preventDefault();
      Mediator.request('app:panel:venues');
    }
  });
});
