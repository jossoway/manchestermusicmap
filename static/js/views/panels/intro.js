define([
  'underscore',
  'marionette',
  'mediator',
  'hbs!templates/panels/intro'
], function(_, Marionette, Mediator, tpl) {

  return Backbone.Marionette.ItemView.extend({
    template: tpl,
    className: 'welcome scrollPanel',

    events: {
      "click #intro-contact":   "displayContact",
    },

    displayContact: function(e) {
      e.preventDefault();
      Mediator.request('app:panel:contact');
    },
  });
});
