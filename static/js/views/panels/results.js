define([
  'jquery',
  'underscore',
  'marionette',
  'mediator',
  'views/event',
  'hbs!templates/panels/results',
  'router',
  'controller',
  'youtube_api',
], function($, _, Marionette, Mediator, EventItemView, tpl, Router, Controller) {

  return Backbone.Marionette.CompositeView.extend({
    template: tpl,
    itemView: EventItemView,
    itemViewContainer: 'ol',
    className: 'resultsPanel scrollPanel',

    events: {
      "click h3 a": "filter",
      "click #search-filters a": "filter"
    },

    initialize: function(attributes, options) {
      // create model to hold results info
      if(options.query === '') {
        this.model = new Backbone.Model({
          showSearchResults: false
        });
      }
      else {
        this.model = new Backbone.Model({
          showSearchResults: true,
          query: options.query,
          hits: this.collection.length
        });
      }

      this.itemCount = 0;
    },

    filter: function(e) {
      e.preventDefault();
      // strip trailing slash, then extract last URL segment
      var href = e.target.href.slice(0, -1);
      var query = decodeURI(href.substr(href.lastIndexOf('/') + 1));
      Controller.search(query);
      Router.navigate('search/' + query);
    },

    itemViewOptions: function(model, index) {
      return {
        itemIndex: index
      };
    },

    onItemviewEventToggle: function(item) {
      var self = this;

      // toggle the currently active item to close it, if there is one. Then toggle this
      // item to open it. First check that we are not dealing with same items
      // Finally, update ref to active item.
      // Before closing an active video, pause it, if it is playing.

      if(this.activeItem) {
        // We already have an active item...
        if(this.activeItem.cid != item.cid) {
          // The previous active item is not this item..

          // determine if thids item is before or after active item
          var isAfter = item.count > self.activeItem.count;

          // pause active video
          this._pauseVideo(this.activeItem);

          // collapse previous active item
          self.activeItem.$el.toggleClass('active').promise().done( function() {
            self.activeItem.$el.find('i:first').toggleClass('icon-rotate-180');

            self.activeItem.$el.find('.result-content').slideToggle();

            // expand newly active item
            item.$el.toggleClass('active').promise().done( function() {
              item.$el.find('i:first').toggleClass('icon-rotate-180');

              item.$el.find('.result-content').slideToggle( function() {
                self._updateMarker(item.model);

                // show video, unless on small device, in which case, show link
                if(self._is_small_device()) {
                  self._appendLink(item);
                }
                else {
                  self._appendVideo(item);
                }

                self.activeItem = item;

                // set focus to  top video panel of active item, as expanding the
                // panel may scroll this off screen. Account for 100px header
                var videoContainer = item.$el.find('.video-container');

                var offset;
                if(isAfter) {
                  offset = videoContainer.offset().top +150;
                }
                else {
                  offset = videoContainer.offset().top -100;
                }

                self.$el.animate({
                  scrollTop: offset
                }, 200);

                Mediator.request('app:panel:resize');
              });
            });
          });
        }
        else {
          // The previous active item is actually this item..
          // if we are collapsing this item, also pause video if required
          // and remove reference to active item
          if(item.$el.hasClass('active')) {
            // this._pauseVideo(this.activeItem);
            // var activePlayer = item.player;

            // if (typeof activePlayer !== 'undefined') {
            //   // playerstate of 1 means video is playing
            //   if(activePlayer.getPlayerState() == 1) {
            //     activePlayer.pauseVideo();
            //   }
            // }
            this.activeItem = null;
          }
          item.$el.toggleClass('active').promise().done( function() {
            item.$el.find('.result-content').slideToggle( function() {
              item.$el.find('i:first').toggleClass('icon-rotate-180');

              Mediator.request('app:panel:resize');
            });
          });
        }
      }
      else {
        // We have no current active item..
        item.$el.toggleClass('active').promise().done( function() {
          item.$el.find('i:first').toggleClass('icon-rotate-180');

          item.$el.find('.result-content').slideToggle( function() {
            self._updateMarker(item.model);

            // show video, unless on small device, in which case, show link
            if(self._is_small_device()) {
              self._appendLink(item);
            }
            else {
              self._appendVideo(item);
            }

            self.activeItem = item;

            Mediator.request('app:panel:resize');
          });
        });
      }
    },

    _pauseVideo: function(item) {
      // var activePlayer = item.player;

      // if (typeof activePlayer !== 'undefined') {
      //   // playerstate of 1 means video is playing
      //   if(activePlayer.getPlayerState() == 1) {
      //     activePlayer.pauseVideo();
      //   }
      // }
    },

    _appendVideo: function(item) {
      // load video if not already loaded, by extracting uid from data element
      var video_container = item.$el.find('.video-container');

      // first see if we already have a video inserted into DOM. The YouTube API
      // will replace the child div of the video-container element
      if($(video_container).find('div:first').children().length === 0) {
        var video_uid = video_container.data('uid');
        // inject video tag
        item.player = new YT.Player(video_container.children(':first')[0], {
          height: '420',
          width: '315',
          videoId: video_uid
        });
      }
    },

    _appendLink: function(item) {
      // load link if not already loaded, by extracting uid from data element
      var video_container = item.$el.find('.video-container');

      // first see if we already have a link inserted into DOM.
      if($(video_container).find('div:first').children().length === 0) {
        var video_uid = video_container.data('uid');
        // inject link
        video_container.children(':first').append('<a class="mobile-link" href="http://www.youtube.com/watch?v=' + video_uid + '"><i class="icon-youtube-play icon-large"></i> Watch Video</a>');
      }
    },

    _updateMarker: function(event) {
      Mediator.request('map:setViewToMarker', event.locationId);
      // also update URL
      Router.navigate('video/' + event.get('slug'));
    },

    _is_small_device: function() {
     return window.screen.width < 541;
     // return true;
    }
  });
});
