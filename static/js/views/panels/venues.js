define([
  'underscore',
  'marionette',
  'router',
  'controller',
  'hbs!templates/panels/venues'
], function(_, Marionette, Router, Controller, tpl) {

  return Backbone.Marionette.ItemView.extend({
    template: tpl,
    className: 'welcome scrollPanel',

    events: {
      "click a": "search"
    },

    initialize: function(attributes, options) {
      this.venues = options.venues;
    },

    serializeData: function() {
      var data = {};
      data.venues = this.venues;
      return data;
    },

    search: function(e) {
      e.preventDefault();
      // strip trailing slash, then extract last URL segment
      var href = e.target.href;
      var query = decodeURI(href.substr(href.lastIndexOf('/') + 1));
      Controller.search(query);
      Router.navigate('search/' + query);
    },
  });
});
