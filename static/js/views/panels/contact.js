define([
  'underscore',
  'marionette',
  'hbs!templates/panels/contact'
], function(_, Marionette, tpl) {

  return Backbone.Marionette.ItemView.extend({
    template: tpl,
    className: 'welcome scrollPanel'
  });
});
