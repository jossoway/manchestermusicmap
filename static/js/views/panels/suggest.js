define([
  'underscore',
  'marionette',
  'mediator',
  'models/suggestion',
  'hbs!templates/panels/suggest'
], function(_, Marionette, Mediator, Suggestion, tpl) {

  return Backbone.Marionette.ItemView.extend({
    template: tpl,
    className: 'welcome scrollPanel',
    model: new Suggestion(),

    ui: {
      form: '#suggest-form',
      errorMsg: '#error-msg',
      sysErrorMsg: '#sys-error-msg',
      successMsg: '#success-msg'
    },

    events: {
      "submit #suggest-form":   "onSubmit",
    },

    onRender: function() {
      var self = this;

      // Seems thatthe ui hash is not available to the valifation callbacks (always)
      this.theForm = this.$el.find('#suggest-form');
      this.errorMsg = this.$el.find('#error-msg');

      Backbone.Validation.bind(this);

      this.model.on('validated:valid', this.valid, this);
      this.model.on('validated:invalid', this.invalid, this);

      this._resetFormErrors();
      this.ui.successMsg.css('display', 'none');
    },

    valid: function(model) {
      Mediator.request('app:panel:resize');
      Mediator.request('app:panel:scrolltop');
    },

    invalid: function(model, errors) {
      var self = this;

      _.each(errors, function(error, field) {
        var input = self.theForm.find('input[name=' + field + ']');
        input.closest('div').addClass('error');
        input.prev().text(error);
      });

      this.errorMsg.css('display', 'block');
      // Mediator.request('app:panel:resize');
      Mediator.request('app:panel:scrolltop');
    },

    _resetFormErrors: function() {
      this.ui.errorMsg.css('display', 'none');
      this.ui.sysErrorMsg.css('display', 'none');

      this.ui.form.find('div').removeClass('error');
      this.ui.form.find('.help-block').text('');
    },

    onSubmit: function(e) {
      var self = this;

      e.preventDefault();

      this._resetFormErrors();

      var data = this._serializeForm();

      this.model.save(data, {
        success: function() {
          self.ui.successMsg.css('display', 'block');
          self.model.clear();
          self.ui.form.find("input[type=text], textarea, email").val('');
        },
        error: function() {
          self.ui.sysErrorMsg.css('display', 'block');
        }
      });
    },

    _serializeForm: function() {
      var o = {};
      var a = this.ui.form.serializeArray();

      $.each(a, function() {
        if (o[this.name] !== undefined) {
          if (!o[this.name].push) {
              o[this.name] = [o[this.name]];
          }
          o[this.name].push(this.value || '');
        } else {
          o[this.name] = this.value || '';
        }
      });
      return o;
    }
  });
});
