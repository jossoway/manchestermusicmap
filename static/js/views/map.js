define([
  'underscore',
  'jquery',
  'constants',
  'marionette',
  'mediator',
  'leaflet',
  'leaflet_markercluster',
  'leaflet_locate',
  'leaflet_providers'
], function(_, $, Constants, Marionette, Mediator) {

  var view = Backbone.Marionette.ItemView.extend({
    // template is empty. All we need is a div with id and class
    template: function() {
      return '';
    },

    id: 'map',
    className: 'leaflet-container',

    DEFAULT_LNG: -2.241692999999941000,
    DEFAULT_LAT: 53.480386000000000000,

    markers: {}, // dictionary of all markers

    events: {
      'click': 'closePanel',
      'tap': 'closePanel'
    },

    closePanel: function(e) {
      // close panel if map is clicked/tapped - unless on a popup marker
      if(e.target.nodeName != 'A') {
        Mediator.request('app:closePanel');
      }
    },

    onRender: function() {
      var self = this;

      L.Icon.Default.imagePath = '/static/images/leaflet';

      var mapEl = this.$el[0];
      var map = L.map(mapEl, { zoomControl: false }).setView([this.DEFAULT_LAT, this.DEFAULT_LNG], 13);

      var defaultLayer = L.tileLayer.provider('OpenStreetMap.Mapnik').addTo(map);

      var baseLayers = {
        'OpenStreetMap Default': defaultLayer,
        'OpenStreetMap Black and White': L.tileLayer.provider('OpenStreetMap.BlackAndWhite'),
        'MapQuest OSM': L.tileLayer.provider('MapQuestOpen.OSM'),
        'Stamen Toner': L.tileLayer.provider('Stamen.Toner'),
        'Stamen Toner Hybrid': L.tileLayer.provider('Stamen.TonerHybrid'),
        'Stamen Toner Lite': L.tileLayer.provider('Stamen.TonerLite'),
        'Esri WorldStreetMap': L.tileLayer.provider('Esri.WorldStreetMap'),
        'Esri WorldGrayCanvas': L.tileLayer.provider('Esri.WorldGrayCanvas'),
        'Nokia Normal Day': L.tileLayer.provider('Nokia.normalDay'),
        'Nokia Normal Day Grey': L.tileLayer.provider('Nokia.normalGreyDay'),
        'Nokia Terrain Day': L.tileLayer.provider('Nokia.terrainDay'),
        'Acetate': L.tileLayer.provider('Acetate'),
        'Acetate Roads': L.tileLayer.provider('Acetate.roads'),
      };

      var overlayLayers = {};

      L.control.layers(baseLayers, overlayLayers, {collapsed: true}).addTo(map);

      L.control.locate({position: 'topright' }).addTo(map);

      // Add positioned zoom control
      map.whenReady(function () {
          new L.Control.Zoom({ position: 'topright' }).addTo(this);
      });

      this.map = map;

      Mediator.setHandler("map:setViewToMarker", function(markerId) {
        self.setViewToMarker(markerId);
      });
    },

    repaint: function() {
      this.map.invalidateSize(false);
    },

    addLocations: function() {
      var self = this;

      var markerClusters = new L.MarkerClusterGroup({
        showCoverageOnHover: false
      });

      // retrieve geojson by AJAX and apply to layer
      var url = '/all_locations/';

      $.getJSON(url, function (data) {
          var partial = [
              '<strong><%= title %></strong>',
              '<p><a href="#" data-location="<%= title %>">View videos</a></p>'
              ].join('');

          var popupTemplate = _.template(partial);

          var locationsLayer = L.geoJson(data, {
              pointToLayer: function (feature, latlng) {
                  // add location to marker
                  // needs assigning as a var as it is referenced outside scope of this method
                  self.locationId = 'l' + feature.properties.id;

                  // var marker = new customCircleMarker(latlng, customMarkerOptions);
                  var marker = new L.Marker(latlng);

                  // store ref to marker keyed on location id used in search index (name)
                  self.markers[self.locationId] = marker;

                  return marker;
              },
              onEachFeature: function (feature, layer) {
                  var location = feature.properties.title;
                  var events = feature.properties.event_list;

                  if (feature.properties) {
                      var popupContent = popupTemplate(feature.properties);
                      layer.bindPopup(popupContent);
                  }

                  // add location to searchbox search terms
                  // split word boundaires into tokens
                  var tokens = location.match(/\w+/g);

                  var doc = {
                      'name': self.locationId,
                      'value': location,
                      'type': Constants.VENUE,
                      'tokens': tokens
                  };

                  Mediator.request('searchbox:addSearchTerm', doc);

                  // add to list of all venues
                  Mediator.request('app:addLocation', location);

                  if (typeof events !== 'undefined' && events.length > 0) {
                    _.each(events, function(event) {
                      _.each(event.artists, function(artist) {
                        // add event details to search index
                        doc = {
                            "id": event.id,
                            "title": event.title,
                            "location": location,
                            "artist": artist.name,
                        };

                        Mediator.request('search-index:addEntry', doc, event, self.locationId);

                        // add artist to search box search terms if not already present
                        var name = 'a' + artist.id;

                        // also add to list of all artists
                        Mediator.request('app:addArtist', artist.name);

                        // split word boundaires into tokens
                        tokens = artist.name.match(/\w+/g);

                        var entry = {
                            'name': name,
                            'value': artist.name,
                            'type': Constants.ARTIST,
                            'tokens': tokens
                        };

                        Mediator.request('searchbox:addSearchTerm', entry);
                      });
                    });
                  }
              }
          });

          markerClusters.addLayer(locationsLayer);
          self.map.addLayer(markerClusters);
          self.markerClusters = markerClusters;

          // setup search box now that it is populated with search terms
          Mediator.request('searchbox:addDataset');

          // sort all artists & all locations now they are fully populated
          Mediator.request('app:sortLists');

          // init router now we have event data populated
          Mediator.request('app:router:init');

          // When marker is selected, add event to anchor to perform search and populate search box
          self.map.on('popupopen', function(e) {
              $('a[data-location]').on('click', function(e) {
                  e.preventDefault();
                  var venue = $(this).data('location');
                  Mediator.request('searchbox:setQuery', venue);

                  results = Mediator.request('search-index:search', venue);
                  Mediator.request('app:panel:events', venue, results);
              });
          });
      });
    },

    setViewToMarker: function(markerId) {
      // get name from index and lookup marker in dictionary based on this key
      var marker = this.markers[markerId];

      // move map to LatLng of this marker and open popup
      this.markerClusters.zoomToShowLayer(marker, function() {
          marker.openPopup();
      });
    }
  });

  return view;
});
