define([
  'jquery',
  'underscore',
  'marionette',
  'hbs!templates/event-item'
], function($, _, Marionette, tpl) {

  return Backbone.Marionette.ItemView.extend({
    template: tpl,
    tagName: 'li',
    className: 'result',
    videoIsPlaying: false,

    events: {
      "click .result-title":   "toggleEventDetails"
    },

    initialize: function(options) {
      this.count = options.itemIndex;
    },

    toggleEventDetails: function(e) {
      e.preventDefault();
      this.trigger("event:toggle", this.model);
    }
  });
});
