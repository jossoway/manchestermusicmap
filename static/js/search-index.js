define([
  'underscore',
  'mediator',
  'models/event',
  'collections/events',
  'lunr'
], function(_, Mediator, Event, Events) {

  return {
    initialize: function() {
      var self = this;

      this.idx = lunr(function() {
        this.ref('id');

        this.field('title');
        this.field('location');
        this.field('artist');
      });

      // create empty collection of events for app
      this.allEvents = new Events();

      Mediator.setHandler("search-index:addEntry", function(doc, event, locationId) {
        self.add(doc, event, locationId);
      });

      Mediator.setHandler("search-index:search", function(query) {
        return self.search(query);
      });

      Mediator.setHandler("search-index:find:slug", function(slug) {
        return self.findBySlug(slug);
      });
    },

    add: function(doc, event, locationId) {
      this.idx.add(doc);
      var entry = new Event(event);
      // record locationId against event
      entry.locationId = locationId;
      this.allEvents.add(entry);
    },

    search: function(query) {
      var self = this;

      // build new collection
      var events = new Events();
      this.idx.search(query).map(function (result) {
        // get event from dict and add to collection
        var event = self.allEvents.get(result.ref);
        events.push(event);
      });

      return events;
    },

    findBySlug: function(slug) {
      var events = new Events();
      results = this.allEvents.where({'slug': slug});

      _.each(results, function (event) {
        events.push(event);
      });

      return events;
    }
  };
});
