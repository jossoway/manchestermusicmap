// Require.js allows us to configure shortcut alias
require.config({
  baseUrl: "/static/js",

  hbs: {
    disableI18n: true,
    helperPathCallback: function (name) {
      return 'templates/helpers/' + name;
    },
  },

  paths: {
    jquery: 'lib/jquery/jquery',
    'jquery.cookie': 'lib/jquery/jquery.cookie',
    underscore: 'lib/underscore',
    json2: 'lib/json2',
    Handlebars: 'lib/Handlebars',
    backbone: 'lib/backbone/backbone',
    csrf: 'lib/backbone/csrf',
    marionette: 'lib/backbone/backbone.marionette',
    'marionette.extensions': 'lib/backbone/marionette-extensions',
    backbone_validation: 'lib/backbone/backbone-validation-min',
    text: 'lib/text',
    hbs: 'lib/hbs',
    lunr: 'lib/lunr.min',
    typeahead: 'lib/typeahead.min',
    leaflet: 'lib/leaflet/leaflet',
    leaflet_markercluster: 'lib/leaflet/leaflet.markercluster',
    leaflet_locate: 'lib/leaflet/L.Control.Locate',
    leaflet_providers: 'lib/leaflet/leaflet-providers',
    youtube_api: 'lib/youtube_iframe_api',
    bootstrap_dropdown: 'lib/bootstrap-dropdowns.min',
  },

  shim: {
    'underscore': {
      exports: '_'
    },
    'marionette': {
      deps: ['backbone'],
      exports: 'Marionette'
    },
    'backbone': {
      deps: ['underscore', 'jquery', 'json2'],
      exports: 'Backbone'
    },
    // ensure that leaflet is loaded before markercluster
    'leaflet_markercluster': {
      deps: ['leaflet']
    },
    'leaflet_locate': {
      deps: ['leaflet']
    },
    'leaflet_providers': {
      deps: ['leaflet']
    },
    'typeahead': ['jquery'],
    'bootstrap_dropdown': ['jquery'],
    'csrf': ['backbone'],
    'backbone_validation': ['backbone']
  }
});

require([
  'app', 'youtube_api',
  'jquery', 'jquery.cookie'
], function(Application) {
  Application.start();
});
