define(['marionette', 'controller'],

function(Marionette, Controller) {
  var Router = Backbone.Marionette.AppRouter.extend({
    appRoutes:{
      ''               : 'index',
      'video/:slug/'   : 'video',
      'search/:query/' : 'search',
      'about/'         : 'about',
      'contact/'       : 'contact',
      'suggest/'       : 'suggest',
      'artists/'       : 'artists',
      'venues/'        : 'venues',
    }
  });

  return new Router({controller: Controller});
});
