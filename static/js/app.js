define([
  'jquery',
  'underscore',
  'marionette',
  'router',
  'mediator',
  'search-index',
  'views/header',
  'views/map',
  'views/panels/intro',
  'views/panels/contact',
  'views/panels/suggest',
  'views/panels/results',
  'views/panels/artists',
  'views/panels/venues',
], function($, _, Marionette, Router, Mediator, SearchIndex,
  HeaderView, MapView, IntroPanelView, ContactPanelView,
  SuggestPanelView, ResultsView, ArtistsPanelView, VenuesPanelView) {

  var MusicMapApp = Backbone.Marionette.Application.extend({
    body: $('body'),
    navPanel: $('#nav'),
    mapPanel: $('#map-container'),

    locations: [],
    artists: [],

    togglePanel: function() {
      var self = this;
      this.navPanel.toggleClass('open').promise().done( function() {
        self.body.toggleClass('open');
      });
    },

    closePanel: function() {
      var self = this;
      this.navPanel.removeClass('open').promise().done( function() {
        self.body.removeClass('open');
      });
    },

    openPanel: function(panelView) {
      var self = this;
      this.navPanel.addClass('open').promise().done( function() {
        self.body.addClass('open');
      });
    },

    displayResultsPanel: function(query, events) {
      // update panel region to show results panel
      var panelView = new ResultsView({
          collection: events
        },{
          query: query
        });

      this.panelRegion.show(panelView);
      this.openPanel(panelView);
    },

    showIntroPanel: function() {
      var panelView = new IntroPanelView();
      this.panelRegion.show(panelView);
      this.openPanel(panelView);

      Router.navigate('');
    },

    showAboutPanel: function() {
      var panelView = new IntroPanelView();
      this.panelRegion.show(panelView);
      this.openPanel(panelView);

      Router.navigate('/about/');
    },

    showContactPanel: function() {
      var panelView = new ContactPanelView();
      this.panelRegion.show(panelView);
      this.openPanel(panelView);

      Router.navigate('/contact/');
    },

    showSuggestPanel: function() {
      var panelView = new SuggestPanelView();
      this.panelRegion.show(panelView);
      this.openPanel(panelView);

      Router.navigate('/suggest/');
    },

    showArtistsPanel: function() {
      var panelView = new ArtistsPanelView([], {
        artists: this.artists
      });
      this.panelRegion.show(panelView);
      this.openPanel(panelView);

      Router.navigate('/artists/');
    },

    showVenuesPanel: function() {
      var panelView = new VenuesPanelView([], {
        venues: this.locations
      });
      this.panelRegion.show(panelView);
      this.openPanel(panelView);

      Router.navigate('/venues/');
    },

    addLocation: function(location) {
      // avoid dupes
      if (_.findWhere(this.locations, location)) {
        // do nothing
      }
      else {
        this.locations.push(location);
      }
    },

    addArtist: function(artist) {
      // avoid dupes
      if (_.findWhere(this.artists, artist)) {
        // do nothing
      }
      else {
        this.artists.push(artist);
      }
    },

    sortLists: function() {
      this.locations.sort();
      this.artists.sort();
    },

    _resizeScrollPanel: function() {
      // $(".scrollPanel").css('height', '100%');
    },

    _scrollPanelToTop: function() {
      $(".scrollPanel").animate({scrollTop: 0}, 250);
    }
  });

  var application = new MusicMapApp();

  application.addInitializer( function() {
    var self = this;
    SearchIndex.initialize();

    this.addRegions({
      headerRegion: "#header",
      mapRegion:    "#map-container",
      panelRegion:  "#nav"
    });

    this.headerRegion.show(new HeaderView());

    this.mapView = new MapView();
    this.mapRegion.show(this.mapView);
    this.mapView.addLocations();

    // initialisation of router is deferred until the map has loaded
    // in all data incl. slugs
    Mediator.setHandler("app:router:init", function() {
      if(Backbone.history) {
        Backbone.history.start({pushState: true, root: '/'});
      }
    });

    Mediator.setHandler("app:togglePanel", function() {
      self.togglePanel();
    });

    Mediator.setHandler("app:closePanel", function() {
      self.closePanel();
    });

    Mediator.setHandler("app:panel:intro", function() {
      self.showIntroPanel();
    });

    Mediator.setHandler("app:panel:about", function() {
      self.showAboutPanel();
    });

    Mediator.setHandler("app:panel:contact", function() {
      self.showContactPanel();
    });

    Mediator.setHandler("app:panel:suggest", function() {
      self.showSuggestPanel();
    });

    Mediator.setHandler("app:panel:artists", function() {
      self.showArtistsPanel();
    });

    Mediator.setHandler("app:panel:venues", function() {
      self.showVenuesPanel();
    });

    Mediator.setHandler("app:panel:events", function(query, events) {
      // display results panel
      self.displayResultsPanel(query, events);
    });

    Mediator.setHandler("app:panel:resize", function() {
      self._resizeScrollPanel();
    });

    Mediator.setHandler("app:panel:scrolltop", function() {
      self._scrollPanelToTop();
    });

    Mediator.setHandler("app:addLocation", function(location) {
      self.addLocation(location);
    });

    Mediator.setHandler("app:addArtist", function(artist) {
      self.addArtist(artist);
    });

    Mediator.setHandler("app:sortLists", function() {
      self.sortLists();
    });
  });

  application.on("initialize:after", function() {
    var self = this;

    // repaint map so tiles are rendered
    this.mapView.repaint();

    window.onresize = function() {
      Mediator.request('app:panel:resize');
    };

    // see http://dev.tenfarms.com/posts/proper-link-handling
    $(document).on("click", "a[href^='/']", function(event) {
      if (!event.altKey && !event.ctrlKey && !event.metaKey && !event.shiftKey) {
        event.preventDefault();
        var url = $(event.currentTarget).attr("href").replace(/^\//, "");
        Router.navigate(url);
      }
    });

    $(document).ready(function () {
      // check for session cookie. If not present, show intro panel
      // and set cookie
      var haz_session = $.cookie('haz_session');

      if (typeof haz_session === 'undefined') {
        self.openPanel();
        $.cookie('haz_session', true, { path: '/' });
      }
    });
  });

  return application;
});
